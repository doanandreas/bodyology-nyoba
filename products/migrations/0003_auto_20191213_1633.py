# Generated by Django 2.2.5 on 2019-12-13 09:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20191207_2154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facecategory',
            name='faceCategory',
            field=models.CharField(choices=[('BERMINYAK', 'BERMINYAK'), ('KERING', 'KERING'), ('JERAWAT', 'JERAWAT'), ('NORMAL', 'NORMAL')], max_length=20),
        ),
        migrations.AlterField(
            model_name='haircategory',
            name='hairCategory',
            field=models.CharField(choices=[('DANDRUFF', 'DANDRUFF'), ('DRY HAIR', 'DRY HAIR'), ('HAIR LOSS', 'HAIR LOSS')], max_length=20),
        ),
        migrations.AlterField(
            model_name='skincategory',
            name='skinCategory',
            field=models.CharField(choices=[('SENSITIF', 'SENSITIF'), ('KERING', 'KERING'), ('NORMAL', 'NORMAL')], max_length=20),
        ),
    ]
