from django.db import models

hairCategory = {
    ('DANDRUFF', 'DANDRUFF'),
    ('DRY HAIR', 'DRY HAIR'),
    ('HAIR LOSS', 'HAIR LOSS')
}

faceCategory = {
    ('JERAWAT', 'JERAWAT'),
    ('BERMINYAK', 'BERMINYAK'),
    ('KERING', 'KERING'),
    ('NORMAL', 'NORMAL'),
}

skinCategory = {
    ('NORMAL', 'NORMAL'),
    ('KERING', 'KERING'),
    ('SENSITIF', 'SENSITIF'),
}
# Create your models here.
class hairCategory(models.Model):
    # nama = models.CharField(max_length=20)
    hairCategory = models.CharField(max_length=20, choices=hairCategory)

    def __str__(self):
        return self.hairCategory

class faceCategory(models.Model):
    # nama = models.CharField(max_length=20)
    faceCategory = models.CharField(max_length=20, choices=faceCategory)

    def __str__(self):
        return self.faceCategory

class skinCategory(models.Model):
    # nama = models.CharField(max_length=20)
    skinCategory = models.CharField(max_length=20, choices=skinCategory)

    def __str__(self):
        return self.skinCategory
