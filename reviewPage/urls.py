from django.urls import path, re_path
from . import views

app_name = 'reviewPage'

urlpatterns = [
    path('<str:produk>/', views.get, name='productReview'),
]