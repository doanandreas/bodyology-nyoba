from django import forms
from django.forms import widgets
from .models import ReviewModel

class ReviewForm(forms.ModelForm):
    class Meta:
        model = ReviewModel
        fields = {'isiReview'}
