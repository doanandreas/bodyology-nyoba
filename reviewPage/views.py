from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from homepage.models import CustomUser
from .forms import ReviewForm
from .models import ReviewModel
from django.http import JsonResponse

def get(request, produk):
    if request.user.is_authenticated:
        u = CustomUser.objects.get(email = request.user.email)
        # response_data = {}
        # print(u.username)
        form = ReviewForm()
        # print("MASUK2")
        # print(request.method)
        if request.method == 'POST':
            # u = CustomUser.objects.get(username = request.user.username)
            # print(u.username)
            # print("MASUKKKKKK")
            orang = u.username
            konten = request.POST.get('isiReview')

            ReviewModel.objects.create(
                namaReview = orang,
                produknya = produk,
                isiReview = konten,
            )

            return JsonResponse({'konten':konten, 'produk':produk, 'orang' : orang})

        reviews = ReviewModel.objects.filter(produknya=produk)
        args = { 
            'reviews' : reviews,
            'produk' : produk,
            'form' : form,
        }
        return render(request, 'reviewPage.html', args)

    else:
        form = ReviewForm()
        reviews = ReviewModel.objects.filter(produknya=produk)
        args = { 
            'reviews' : reviews,
            'produk' : produk,
            'form' : form,
        }
        return render(request, 'reviewPage.html', args)


