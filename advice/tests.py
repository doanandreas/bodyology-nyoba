from django.test import TestCase, Client
from django.urls import resolve
from .views import index_advice, terima_advice
from .models import AdviceModel

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

# Create your tests here.
class AdviceTest(TestCase):
    def test_apakah_url_benar_ke_advice(self):
        c = Client()
        response = c.get('/advice/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('advice/index.html')

    def test_apakah_url_advice_menggunakan_fungsi_terima_advice(self):
        found = resolve('/advice/')
        self.assertEqual(found.func, terima_advice)

    def test_apakah_ada_heading(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('We Need Your Advice', content)

    def test_apakah_ada_jumbotron(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('<section class="jumbotron">', content)

    def test_apakah_ada_tombol_submit(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('<input', content)
        self.assertIn('Send Your Advice', content)

class FunctionalTestAdvice(StaticLiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    def test_ada_title_website(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)

        # User melihat title website yang dibuka
        self.assertIn('BODYOLOGY', self.browser.title)
    
    def test_bisa_isi_advice(self):
        # User membuka browser dengan mengakses url advice
        self.browser.get(self.live_server_url + '/advice')

        namaUser = self.browser.find_element_by_name('namaUser')
        isiAdvice = self.browser.find_element_by_name('isiAdvice')

        #Memasukkan advice ke form
        namaUser.send_keys('anon')
        isiAdvice.send_keys('keren')
        
        button = self.browser.find_element_by_id('submit-button')
        button.click()
