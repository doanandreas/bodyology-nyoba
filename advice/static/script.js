$(document).ready(function(){
    console.log("masuk js");
    $('#advice').empty()
    let pesan = document.createElement('h3')
    pesan.innerText = "Loading..."
    $('#advice').append(pesan)

    $.ajax({
        method:'GET',
        url:'getData/',
        success: function(response){
            $('#advice').empty()
            for(let i = 0; i < response.length; i++){
                console.log("data ke " + i );
                console.log(response[i].fields.categoryAdvice)
                console.log(response[i].fields.isiAdvice)
                let nama = document.createElement('h2')
                let category = document.createElement('h3')
                let isiAdvice = document.createElement('h4')
                let garis = document.createElement('hr')
                nama.innerText = response[i].fields.namaUser
                category.innerText = response[i].fields.categoryAdvice
                isiAdvice.innerText = response[i].fields.isiAdvice
                $('#advice').append(nama)
                $('#advice').append(category)
                $('#advice').append(isiAdvice)
                $('#advice').append(garis)
            }
        }
    });
});

$('#submit-button').hover(
    function(){
        $(this).addClass("btn-primary");
    }, function(){
        $(this).removeClass("btn-primary");    
    }
)

$('#submit-button').click(function(){
    $('#thank').append("<b>Thanks for your advice^^</b>");
})
