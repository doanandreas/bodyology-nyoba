from django.shortcuts import render, redirect, HttpResponse
from django.core import serializers
from .forms import AdviceForm
from .models import AdviceModel

# Create your views here.

    

def getData(request):
    dataAdvice = AdviceModel.objects.all()
    advice_list = serializers.serialize('json', dataAdvice)
    return HttpResponse(advice_list, content_type="text/json-comment-filtered")

