from django.urls import path
from . import views
from .views import getData, terima_advice

app_name = 'advice'

urlpatterns = [
    path('', views.terima_advice, name='index'),
    path('getData/', views.getData, name='getData'),
]
