from django.urls import path
from .views import home, logout_view, login_view

app_name = 'homepage'

urlpatterns = [
    path('', home, name='home'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
]